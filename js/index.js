$( document ).ready(function() {
        setTimeout(function() {
            $('.page-loader').addClass('loaded');
            setTimeout(function() {
            	$('body').addClass('body-loaded');
            	$('.subBackground').css({opacity: 0, visibility: "visible"}).animate({opacity: 1}, 1000);
            }, 2000);
        }, 1000);
});
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if(isMobile.any()) {
   $('body').addClass('mobile');
} else $('body').addClass('desktop');
// Resize
$(document).ready(function(){
	var bodyWidth = $('body').width();
    if ( bodyWidth >= '975' ) {
    newHeight = bodyWidth/2;
    } else newHeight = bodyWidth;
    $.each($('.winesBox'), function (index) { 
    $(this).height(newHeight);
    });
});
$(window).resize(function() {
    var bodyWidth = $('body').width();
    if ( bodyWidth >= '975' ) {
    newHeight = bodyWidth/2;
    } else newHeight = bodyWidth;
    $.each($('.winesBox'), function (index) { 
    $(this).height(newHeight);
    });
});
// Scroll hr
function scrollHr( st ) {
    // For Menu
    var about = $("#about").offset().top - $('header').height();
    var border = $("#border").offset().top - $('header').height();
    var redWine = $("#redWine").offset().top - $('header').height();
    var whiteWine = $("#whiteWine").offset().top - $('header').height();
    var roseWine = $("#roseWine").offset().top - $('header').height();
    var story = $("#story").offset().top - $('header').height();
    var clients = $("#clients").offset().top - $('header').height();
    var contact = $("#contact").offset().top - $('header').height();
    // Check
    if ( st >= about ) {
        $("hr").css("width", $(".aboutNav").width());
        $("hr").css("opacity", "1");
        bin = 100 * ( $(".aboutNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
        $("hr").css("left", bin + '%');
        if ( st >= border ) {
            $("hr").css("width", $(".shopNav").width());
            bin = 100 * ( $(".shopNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
            $("hr").css("left", bin + '%');
            if ( st >= redWine ) {
                $("hr").css("width", $(".redNav").width());
                bin = 100 * ( $(".redNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                $("hr").css("left", bin + '%');
                if ( st >= whiteWine ) {
                    $("hr").css("width", $(".whiteNav").width());
                    bin = 100 * ( $(".whiteNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                    $("hr").css("left", bin + '%');
                    if ( st >= roseWine) {
                        $("hr").css("width", $(".roseNav").width());
                        bin = 100 * ( $(".roseNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                        $("hr").css("left", bin + '%');
                        if ( st >= story ) {
                            $("hr").css("width", $(".storyNav").width());
                            bin = 100 * ( $(".storyNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                            $("hr").css("left", bin + '%');
                            if ( st >= clients ) {
                                $("hr").css("width", $(".clientsNav").width());
                                bin = 100 * ( $(".clientsNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                                $("hr").css("left", bin + '%');
                                if ( st >= contact ) {
                                    $("hr").css("width", $(".contactNav").width());
                                    bin = 100 * ( $(".contactNav").offset().left - $(".headerRow").offset().left ) / $("#navUL").width();
                                    $("hr").css("left", bin + '%');
                                }
                            }
                        }
                    }
                }
            }
        } 
        } else {
            $("hr").css("left", "0px");
            $("hr").css("opacity", "0");
        }
}
// Scroll Resize Check
$(window).resize(function() {
    windowWidth = $(window).width();
    if ( windowWidth >= '992' ) {
    sTop = $(window).scrollTop();
    if ( sTop != 0 ) {
     $('header').addClass('fixedHeader');
    } else $('header').removeClass('fixedHeader');
    scrollHr( sTop );
    } else {
    if ( sTop != 0 ) {
     $('#mobileMenu').addClass('fixedMobileMenu');
     if ( $(".hamburger").hasClass("is-active") == true ) {
        $("#mobileMenu").addClass("openMobileMenu");
     }
    } else {
        $("#mobileMenu").removeClass("openMobileMenu");
        $('#mobileMenu').removeClass('fixedMobileMenu');
    } 
    }
});
// Scroll
/**
  *Smooth Scrool
  */
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        windowWidth = $(window).width();
        if ( windowWidth >= '992' ) {
        if ( target.offset().top == $("#clients").offset().top ) {
        $('html, body').animate({
          scrollTop: target.offset().top - $('header').height() + $('#clients').find('svg').height()
        }, 1000);
        } else {
        if ( target.offset().top == $("#articleHome").offset().top ) {
        $('html, body').animate({
          scrollTop: target.offset().top - $('header').height()
        }, 1000);  
        } else {
        $('html, body').animate({
          scrollTop: target.offset().top - $('header').height() + 1
        }, 1000);
        }  
        }
        } else {
        if ( $(".hamburger").hasClass("is-active") == true ) {
        $(".hamburger").toggleClass("is-active");
        $("#mobileMenu").toggleClass("openMobileMenu");
        }
        if ( target.offset().top == $("#clients").offset().top ) {
        $('html, body').animate({
          scrollTop: target.offset().top - $('.mobileMenuHolder').height() + $('#clients').find('svg').height()
        }, 1000);
        } else {
        $('html, body').animate({
          scrollTop: target.offset().top - $('.mobileMenuHolder').height()
        }, 1000);
        }
        }
        return false;
      }
    }
  });
});
// Hide Header on on scroll down
var ScrollBin = 0;
var didScroll;

$(window).scroll(function(event){
    windowWidth = $(window).width();
    if ( windowWidth >= '992' ) {
    var st = $(this).scrollTop();
    if ( ScrollBin == 0 ) {
    if ( st == 0 ) {
    $('header').addClass('fixedHeader');
    }
    ScrollBin = 1;
    } else if ( ScrollBin == 1 ) {
        if ( st == 0 ) {
            $('header').removeClass('fixedHeader');
        }
    }
    scrollHr( st );
    didScroll = true;
    } else {
    var st = $(this).scrollTop();
    if ( ScrollBin == 0 ) {
    if ( st == 0 ) {
    $('#mobileMenu').addClass('fixedMobileMenu');
    if ( $(".hamburger").hasClass("is-active") == true ) {
        $("#mobileMenu").addClass("openMobileMenu");
    }
    }
    ScrollBin = 1;
    } else if ( ScrollBin == 1 ) {
        if ( st == 0 ) {
            $("#mobileMenu").removeClass("openMobileMenu");
            $('#mobileMenu').removeClass('fixedMobileMenu');
        }
    }
    didScroll = true;
    }
});

$(document).ready(function(){
    windowWidth = $(window).width();
    sTop = $(window).scrollTop();
    if ( windowWidth >= '992' ) {
    if ( sTop != 0 ) {
     $('header').addClass('fixedHeader');
    } else $('header').removeClass('fixedHeader');
    scrollHr( sTop );
    } else {
    if ( sTop != 0 ) {
     $('#mobileMenu').addClass('fixedMobileMenu');
     if ( $(".hamburger").hasClass("is-active") == true ) {
        $("#mobileMenu").addClass("openMobileMenu");
     }
    } else {
        $("#mobileMenu").removeClass("openMobileMenu");
        $('#mobileMenu').removeClass('fixedMobileMenu');
    } 
    }
});

setInterval(function() {   
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 0);

function hasScrolled() {
    var st = $(this).scrollTop();
    windowWidth = $(window).width();
    if ( windowWidth >= '992' ) {
    if ( st != 0 ) {
     $('header').addClass('fixedHeader');
    } else $('header').removeClass('fixedHeader');
    } else {
    if ( st != 0 ) {
     $('#mobileMenu').addClass('fixedMobileMenu');
     if ( $(".hamburger").hasClass("is-active") == true ) {
        $("#mobileMenu").addClass("openMobileMenu");
     } 
    } else {
        $("#mobileMenu").removeClass("openMobileMenu");
        $('#mobileMenu').removeClass('fixedMobileMenu');
    }  
    }
}
// Mobile Image Click
$(document).ready(function() {
if ( $("body").hasClass("mobile") == true ) {
$( ".wineImage" ).on("click", "div", function (e) {
    e.preventDefault();
    $(this).toggleClass("mobileWineImageOpen");
});
};
});