$( document ).ready(function() {
        setTimeout(function() {
            //$('.page-loader').addClass('loaded');
            setTimeout(function() {
            	$('body').addClass('body-loaded');
            	$('.subBackground').css({opacity: 0, visibility: "visible"}).animate({opacity: 1}, 1000);
            }, 200);
        }, 1000);
});
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
if(isMobile.any()) {
   $('body').addClass('mobile');
} else $('body').addClass('desktop');
// Resize
$(document).ready(function(){
	var bodyWidth = $('body').width();
    if ( bodyWidth >= '992' ) {
    newHeight = bodyWidth/2;
    } else newHeight = bodyWidth;
    $.each($('.winesBox'), function (index) { 
    $(this).height(newHeight);
    });
});
$(window).resize(function() {
    var bodyWidth = $('body').width();
    if ( bodyWidth >= '992' ) {
    newHeight = bodyWidth/2;
    } else newHeight = bodyWidth;
    $.each($('.winesBox'), function (index) { 
    $(this).height(newHeight);
    });
});